                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
						<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="mr-2 d-none d-lg-inline">
								{{ Auth::user()->name }}
							</span>
							<img class="img-profile rounded-circle" src="{{ asset('images/img_avatar3.png') }}">
						</a>
						<!-- Dropdown - User Information -->
						<ul class="dropdown-menu animated--grow-in rounded-0">
							<li>
								<a class="dropdown-item" href="{{ url('profile', Auth::user()->user_id) }}">
									<i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
									{{ trans('dashboard.profile') }}
								</a>
							</li>

							<div class="dropdown-divider"></div>
							
							<a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                {{ trans('dashboard.logout') }}
                            </a>
						
						</ul>
					</li>