	<header id="header">

		<nav class="navbar navbar-expand-md navbar-dark navbar-hover" style="background-color: #034EA2;">
			<a class="navbar-brand" href="{{ Auth::check() ? url()->current() : url('/') }}">
				<img src="{{ asset('images/logo.png') }}">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHover" aria-controls="navbar" aria-expanded="false" aria-label="Navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarHover">				
				<ul class="navright navbar-nav ml-auto navbar-dark">
					@if(Auth::check())
						@include('admin._partials.userinfo')
					@endif
				</ul>
			</div>
		</nav>
	</header>