    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('sbtemplate/vendor/jquery/jquery.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script src="{{ asset('sbtemplate/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{ asset('sbtemplate/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('sbtemplate/js/sb-admin-2.min.js') }}"></script>
    <script>
		$(document).ready(function(){
			// Add minus icon for collapse element which is open by default
			$(".collapse.show").each(function(){
				$(this).prev(".card-header").find(".fas").addClass("fa-minus").removeClass("fa-plus");
			});
			
			// Toggle plus minus icon on show hide of collapse element
			$(".collapse").on('show.bs.collapse', function(){
				$(this).prev(".card-header").find(".fas").removeClass("fa-plus").addClass("fa-minus");
			}).on('hide.bs.collapse', function(){
				$(this).prev(".card-header").find(".fas").removeClass("fa-minus").addClass("fa-plus");
			});

			// Add Collapse transaction
			$(".trans.show").each(function(){
				$(this).prev(".card-header").find(".fas").addClass("fa-caret-square-up").removeClass("fa-caret-square-down");
			});
			
			// Toggle plus minus icon on show hide of collapse element
			$(".trans").on('show.bs.collapse', function(){
				$(this).prev(".card-header").find(".fas").removeClass("fa-caret-square-down").addClass("fa-caret-square-up");
			}).on('hide.bs.collapse', function(){
				$(this).prev(".card-header").find(".fas").removeClass("fa-caret-square-up").addClass("fa-caret-square-down");
			});

			// Message Alert
			setTimeout(function() { $(".inner-message").removeClass('slideInDown').addClass('slideOutUp'); }, 2000);
			setTimeout(function() { $(".inner-message > button").click(); }, 4000);

			//Select Option Search
			$('.select2').select2({
				placeholder: "{{ trans('dashboard.select') }}",
			});
			$('.select2-multi').select2({
				placeholder: "{{ trans('dashboard.select') }}",
			});

			//Dropdown Menu
			$( '.dropdown-menu .dropdown-toggle' ).on('click', function() {
				var $el = $(this);
				var $parent = $el.offsetParent(".dropdown-menu");
				
				if (!$el.next().hasClass("show")) {
					$el.parents('.dropdown-menu').first().find(".show").removeClass("show");
				}
				$el.next(".dropdown-menu").toggleClass("show").parent("li").toggleClass("show");
				
				$el.parents("li.nav-item.dropdown.show").on("hidden.bs.dropdown", function () {
					$(".dropdown-menu .show").removeClass("show");
				});
				
				if (!$parent.parent().hasClass("navbar-nav")) {
					$el.next().css({"top":$el[0].offsetTop,"left":$parent.outerWidth()});
				}
				
				return false;
			});

			//Fixed Navbar
			$('#header').addClass("fixed-nav");
			// Add the following code if you want the name of the upload file appear on select
			$(".custom-file-input").on("change", function() {
				var fileName = $(this).val().split("\\").pop();
				$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
			});
		});
	</script>