
@if (Session::has('flash_success'))
<div id="message">
  <div class="container">
    <div class="col-md-12">
      <div class="inner-message alert alert-success text-center animated slideInDown">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <!-- <span aria-hidden="true">&times;</span> -->
        </button>
        <strong>
          {{ Session::get('flash_success') }}
        </strong>
      </div>
    </div>
  </div>
</div>
@endif

@if (Session::has('flash_info'))
<div id="message">
  <div class="container">
    <div class="col-md-12">
      <!-- <div class="inner-message alert alert-info text-center animated slideInDown"> -->
      <div class="info-message alert alert-info text-center animated slideInDown">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>
          {{ Session::get('flash_info') }}
        </strong>
      </div>
    </div>
  </div>
</div>
@endif

@if (Session::has('flash_errors'))
<div id="message">
  <div class="container">
    <div class="col-md-12">
      <div class="inner-message alert alert-danger text-center animated slideInDown">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <!-- <span aria-hidden="true">&times;</span> -->
        </button>
        <strong>
          {{ Session::get('flash_errors') }}
        </strong>
      </div>
    </div>  
  </div>
</div>
@endif

@if (Session::has('flash_warning'))
<div id="message">
  <div class="container">
    <div class="col-md-12">
      <div class="inner-message alert alert-warning text-center animated slideInDown">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <!-- <span aria-hidden="true">&times;</span> -->
        </button>
        <strong>
          {{ Session::get('flash_warning') }}
        </strong>
      </div>
    </div>  
  </div>
</div>
@endif