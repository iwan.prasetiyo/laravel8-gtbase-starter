  	<!-- Sidebar -->
    <ul class="navbar-nav bg-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      	<!-- Nav Item - Dashboard -->
      	<li class="nav-item {{ !Request::is('dashboard') ?: 'active' }}">
        	<a class="nav-link" href="{{ route('home') }}">
          		<i class="fas fa-fw fa-tachometer-alt"></i>
          	<span>{{ trans('dashboard.das_sidebar_title') }}</span></a>
      	</li>

      	<!-- Divider -->
      	<hr class="sidebar-divider">
		
		<!-- Nav Item - Utilities Collapse Menu -->
		<li class="nav-item">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
				aria-expanded="true" aria-controls="collapseUtilities">
				<i class="fas fa-fw fa-wrench"></i>
				<span>Utilities</span>
			</a>
			<div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
				data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<h6 class="collapse-header">Custom Utilities:</h6>
					<a class="collapse-item" href="{{ url('animations') }}">Animations</a>
					<a class="collapse-item" href="{{ url('blank') }}">Blank</a>
					<a class="collapse-item" href="{{ url('borders') }}">Borders</a>
					<a class="collapse-item" href="{{ url('buttons') }}">Buttons</a>
					<a class="collapse-item" href="{{ url('cards') }}">Cards</a>
					<a class="collapse-item" href="{{ url('charts') }}">Charts</a>
					<a class="collapse-item" href="{{ url('colors') }}">Colors</a>
					<a class="collapse-item" href="{{ url('others') }}">Other</a>
					<a class="collapse-item" href="{{ url('tables') }}">Tables</a>
					<a class="collapse-item" href="{{ route('404.sample') }}">404</a>
				</div>
			</div>
		</li>

		<!-- Divider -->
		<hr class="sidebar-divider d-none d-md-block">

		<!-- Sidebar Toggler (Sidebar) -->
		<div class="text-center d-none d-md-inline">
			<button class="rounded-circle border-0" id="sidebarToggle"></button>
		</div>

    </ul>
    <!-- End of Sidebar -->