		<div id="admin-copyright">
			<div>
				<a style="color: #999;" href="{{ url('lang/en') }}"><span class="flag-icon flag-icon-gb mr-1"></span>English</a> | <a style="color: #999;" href="{{ url('lang/id') }}">Bahasa<span class="flag-icon flag-icon-id ml-1"></span></a>
			</div>
			<span>Copyright by gtbase &copy; {{ date('Y') }}</span>
		</div>
