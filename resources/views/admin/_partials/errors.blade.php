@if (session()->has('errors'))
<div id="message">
  <div class="container">
    <div class="col-md-12">
      <div class="alert alert-danger text-center animated slideInDown">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>
          {!! session()->get('errors') !!}
        </strong>
      </div>
    </div>  
  </div>
</div>
@endif