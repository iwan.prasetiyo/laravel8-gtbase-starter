<div id="collapse{{ $collapseId }}" class="collapse {{ (Sidebar::getActiveMenuId() == $list_menu->menu_id) ? 'show' : '' }}" aria-labelledby="headingPages" data-parent="#accordionSidebar">
	<div class="bg-white py-2 collapse-inner rounded">
		@foreach (Sidebar::getAllSubmenu($list_menu->menu_id) as $list_submenu)
			@php
				$nameUrl = explode('/', $list_submenu->url)[1];
			@endphp
			<a class="collapse-item {{ active($nameUrl) }}" href="{{ url($list_submenu->url) }}">{{ trans('dashboard.menu.' . $list_submenu->slug) }}</a>
		@endforeach
	</div>
</div>