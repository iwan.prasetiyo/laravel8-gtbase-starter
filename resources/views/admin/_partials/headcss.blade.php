<!-- Custom fonts for this template-->
    <link href="{{ asset('sbtemplate/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Custom styles for this template-->
    <!-- Styles -->
    <link href="{{ asset('sbtemplate/css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/appadmin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2-custom.css') }}" rel="stylesheet">
    @if( env('APP_DEBUG') == true)
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    @else
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    @endif
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/css/flag-icon.min.css" rel="stylesheet">