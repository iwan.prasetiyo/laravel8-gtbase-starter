@extends('layouts.backend')

@section('content')

	<!-- Nav Item - Breadcrumb -->
	@component('admin._partials.breadcrumb')
		@slot('list')
			<li class="breadcrumb-item active" aria-current="page">404 Errors</li>
		@endslot
	@endcomponent

	<!-- 404 Error Text -->
	<div class="text-center">
		<div class="error mx-auto" data-text="404">404</div>
		<p class="lead text-gray-800 mb-5">Page Not Found</p>
		<p class="text-gray-500 mb-0">It looks like you found a glitch in the matrix...</p>
		<a href="{{ url('home') }}">&larr; Back to Dashboard</a>
	</div>

@endsection

@section('js')
	<!-- Core plugin JavaScript-->
    <script src="{{ asset('sbtemplate/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
@endsection