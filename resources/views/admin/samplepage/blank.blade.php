@extends('layouts.backend')

@section('content')

	<!-- Nav Item - Breadcrumb -->
	@component('admin._partials.breadcrumb')
		@slot('list')
			<li class="breadcrumb-item active" aria-current="page">Blank</li>
		@endslot
	@endcomponent

	<!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800">Blank Page</h1>

	{!! Form::open(['url' => 'foo/bar']) !!}
		{!! Form::label('name', 'Name', ['class' => 'awesome']); !!}
		{!! Form::text('name', 'Jake Fisbach', ['class' => 'form-control']); !!}
		
		{{ Form::label('email', 'E-Mail Address', ['class' => 'awesome']); }}
		{!! Form::email('Email', '', ['class' => 'form-control']); !!}
	{!! Form::close() !!}

@endsection