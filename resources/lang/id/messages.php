<?php
return [
	"login" => "Selamat datang :name!!",
	"logout" => "Anda telah keluar!!",
	"save" => "Berhasil Simpan!!",
	"edit" => "Berhasil Ubah!!",
	"delete" => "Berhasil Hapus!!",
	"approve" => "Berhasil Approve!!",
	"support" => "Berhasil kirim reminder!!",
	"support_error" => "Waktu respon sudah ada!!",
	"approve_error" => "Maaf :name akun anda belum di approve. Silahkan hubungi administrator ( :mail ).",
	"session_expired" => "Sesi Anda kedaluwarsa. Silahkan masuk untuk melanjutkan.",
	"page_notfound" => "Halaman yang Anda cari tidak ditemukan.",	
];