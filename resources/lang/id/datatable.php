<?php
return [
	"sProcessing" =>   "Sedang memproses...",
	"sLengthMenu" =>   "Tampilkan _MENU_ entri",
	"sZeroRecords" =>  "Tidak ditemukan data yang sesuai",
	"sInfo" =>         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
	"sEmptyTable" =>   "Tak ada data yang tersedia pada tabel ini",
	"sInfoEmpty" =>    "Menampilkan 0 sampai 0 dari 0 entri",
	"sInfoFiltered" => "(disaring dari _MAX_ entri keseluruhan)",
	"sInfoPostFix" =>  "",
	"sLoadingRecords" => "Memuat...",
	"sSearch" =>       "Cari:",
	"sFirst" =>    "Pertama",
	"sPrevious" => "Sebelumnya",
	"sNext" =>     "Selanjutnya",
	"sLast" =>     "Terakhir",

];