<?php
return [
	"welcome_title" => "Selamat datang di GTBASE CMS",
	"lang" 		=> "Bahasa",
	"profile" 	=> "Profil",
	"image" 	=> "Gambar",
	"logout" 	=> "Keluar",
	"search" 	=> "Cari",
	"create" 	=> "Buat",
	"detail" 	=> "Detil",
	"add" 		=> "Tambah",
	"edit" 		=> "Ubah",
	"view" 		=> "Lihat",
	"manage"	=> "Kelola",
	"submit" 	=> "Kirim",
	"save" 		=> "Simpan",
	"cancel" 	=> "Batal",
	"clear" 	=> "Bersihkan",
	"delete" 	=> "Hapus",
	"back" 		=> "Kembali",
	"yes" 		=> "Ya",
	"no" 		=> "Tidak",
	"empty" 	=> "Kosong",
	"from" 		=> "Dari",
	"to" 		=> "Ke",
	"date" 		=> "Tanggal",
	"method" 	=> "Metode",
	"created_at" 	=> "Tanggal Dibuat",
	"code" 		=> "Kode",
	"choose_file" 	=>	"Pilih File",
	"description" 	=> "Deskripsi",
	"country" 	=> "Negara",
	"city" 		=> "Kota",
	"ubah_password" 	=> "Ubah Password",
	"old_password" 		=> "Kata sandi Lama",
	"new_password" 		=> "Kata sandi Baru",
	"password"			=> "Kata sandi",
	"confirm_password"	=> "Konfirmasi Kata sandi",
	"confirm_logout"	=> "Anda yakin ingin keluar?",
	"logout_message"	=> "Pilih :message jika Anda siap untuk mengakhiri sesi saat ini.",
	"access_menu"	=> "Menu",
	"update_password" => "Jika tidak mengganti password, biarkan kosong.",
	"delete_desc" => "Apakah kamu yakin ingin menghapus item ini ?",

	/**
	 * ---------------------------------------------------
	 * Dashboard @prefix with (das_)
	 * ---------------------------------------------------
	 */
	"das_sidebar_title" => "Beranda",

	/**
	 * ---------------------------------------------------
	 * Master Data @prefix with (mst_)
	 * ---------------------------------------------------
	 */
	"mst" => [
		"sidebar_title"			=> "Data Master",
		"sidebar_menu" 			=> "Menu",
		"sidebar_category" 		=> "Kategori",
		"sidebar_distributor" 	=> "Distributor",
		"sidebar_group_product" => "Group Product",
	],

	/**
	 * ---------------------------------------------------
	 * Log History @prefix with (log_)
	 * ---------------------------------------------------
	 */

	"menu" => [
		"manage_title"				=> "Manajemen User",
		"manage_user_groups"		=> "User Grup",
		"manage_users" 				=> "User",

		"settings_title"			=> "Pengaturan",
		"settings_category" 		=> "Kategori",
		"settings_menu" 			=> "Menu",
		"settings_roles" 			=> "Peran",
		"settings_roles_user" 		=> "Peran Pengguna",
		"settings_responsibility"	=> "Tanggung Jawab",

		"admin_support_title"		=> "Admin Support",
		"admin_category" 			=> "Kategori",
		"admin_officer" 			=> "Officer",

		"customer_title"			=> "Customer",
		"product_title"				=> "Website",
		"officer_title"				=> "Officer",
		"support_title"				=> "Support",
		"inquiries_title"			=> "Inquiries",
		"support_officer"			=> "Support Officer",
		"category_officer"			=> "Kategori Officer",
		"history_title"			=> "Histori Log",
	],

	"status"	=>  "Status",
	"email"		=>  "Email",
	"product"	=>  "Website",
	"support"	=>  "Kategori",
	"inquiries"	=>  "Inquiries",
	"reminder_time"	=>  "Waktu Reminder",
	"input_respon_time"	=>  "Isi Waktu Response",
	"respon_time"	=>  "Waktu Respon",
	"response"	=>  "Respon",
	"customer"	=>  "Customer",
	"officer"	=>  "Officer",
	"website"	=>  "Website",
	"select"	=>  "-- Pilih --",
	"active"	=>	"Aktif",
	"inactive"	=>	"Tidak Aktif",
	"actions"	=>  "Aksi",
	"upload"	=>  "Unggah",
	"download"	=>  "Unduh",
	"file_upload" 		=>	"Pilih file unggahan:",
	"selectfile_import" => "Pilih file untuk di import:",
	"success_import" => "Data anda berhasil di import",
	"error_import" => "Insert data Gagal.",
	//Category
	"name" 		=>  "Nama",
	"parent" 	=>	"Induk",
	//group user
	"category" 		=>  "Kategori",
	"company" 	=>	"Perusahaan",
	"parent_company" 	=>	"Perusahaan induk",
	"address" 	=>	"Alamat",
	"type" 	=>	"Tipe",
	//user
	"username" 	=>	"Nama user",
	"phone" 	=>	"Telepon",
	"role" 		=>	"Peran",
	"send"		=>	"Kirim email",
	//menu
	"parent_menu"	=>	"Menu Induk",
	"order" 		=>	"Urutan",
	"level_menu" 	=>	"Menu Level",
	"icon" 		=>	"Ikon",
	"is_link" 	=>	"Tautan",
	//role
	"fullname" 	=>	"Nama",
	"role_name"	=>	"Peran User",

];