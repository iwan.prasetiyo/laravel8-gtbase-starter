<?php
return [
	"login" => "Welcome :name!!",
	"logout" => "You are logged out!!",
	"save" => "Save Success!!",
	"edit" => "Edit Success!!",
	"delete" => "Delete Success!!",
	"approve" => "Approve Success!!",
	"support" => "Send Reminder Success!!",
	"support_error" => "Respon time already!!",
	"approve_error" => "Sorry :name your account not approve. Please contact administrator ( :mail ).",
	"session_expired" => "Your session expired. Please login to continue.",
	"page_notfound" => "The page you are looking for was not found.",
];