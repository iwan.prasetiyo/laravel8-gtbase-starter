<?php
return [
	"welcome_title" => "Welcome to GTBASE CMS",
	"lang" 		=> "Language",
	"profile" 	=> "Profile",
	"image" 	=> "Image",
	"logout" 	=> "Logout",
	"search" 	=> "Search",
	"create" 	=> "Create",
	"detail" 	=> "Detail",
	"add" 		=> "Add",
	"edit" 		=> "Edit",
	"view" 		=> "View",
	"manage" 	=> "Manage",
	"submit" 	=> "Submit",
	"save" 		=> "Save",
	"cancel" 	=> "Cancel",
	"clear" 	=> "Clear",
	"delete" 	=> "Delete",
	"back" 		=> "Back",
	"yes" 		=> "Yes",
	"no" 		=> "No",
	"empty" 	=> "Empty",
	"from" 		=> "From",
	"to" 		=> "To",
	"date" 		=> "Date",
	"method" 	=> "Method",
	"created_at" 	=> "Created Date",
	"code" 		=> "Code",
	"choose_file" 	=>	"Choose File",
	"description" 	=> "Description",
	"country" 	=> "Country",
	"city" 		=> "City",
	"ubah_password" 	=> "Change Password",
	"old_password" 		=> "Old Password",
	"new_password" 		=> "New Password",
	"password" 			=> "Password",
	"confirm_password" 	=> "Confirm Password",
	"confirm_logout"	=> "Are you sure to Leave?",
	"logout_message"	=> "Select :message if you are ready to end current session.",
	"access_menu"	=> "Menu",
	"update_password" => "If you do not change the password, leave it blank.",
	"delete_desc" => "Are you sure you want to delete this item ?",

	/**
	 * ---------------------------------------------------
	 * Dashboard @prefix with (das_)
	 * ---------------------------------------------------
	 */
	"das_sidebar_title" => "Dashboard",

	/**
	 * ---------------------------------------------------
	 * Master Data @prefix with (mst_)
	 * ---------------------------------------------------
	 */
	"mst" => [
		"sidebar_title"			=> "Master Data",
		"sidebar_menu" 			=> "Menu",
		"sidebar_category" 		=> "Category",
		"sidebar_distributor" 	=> "Distributor",
		"sidebar_group_product" => "Group Product",
	],

	/**
	 * ---------------------------------------------------
	 * Log History @prefix with (log_)
	 * ---------------------------------------------------
	 */

	"menu" => [
		"manage_title"				=> "User Management",
		"manage_user_groups"		=> "User Group",
		"manage_users" 				=> "User",

		"settings_title"			=> "Settings",
		"settings_category" 		=> "Category",
		"settings_menu" 			=> "Menu",
		"settings_roles" 			=> "Roles",
		"settings_roles_user" 		=> "Roles User",
		"settings_responsibility"	=> "Responsibility",

		"admin_support_title"		=> "Admin Support",
		"admin_category" 			=> "Category",
		"admin_officer" 			=> "Officer",

		"customer_title"			=> "Customer",
		"product_title"				=> "Product",
		"officer_title"				=> "Officer",
		"support_title"				=> "Support",
		"inquiries_title"			=> "Inquiries",
		"support_officer"			=> "Support Officer",
		"category_officer"			=> "Category Officer",
		"history_title"			=> "Log History",
	],

	"status"	=>  "Status",
	"email"		=>  "Email",
	"product"	=>  "Website",
	"support"	=>  "Category",
	"inquiries"	=>  "Inquiries",
	"reminder_time"	=>  "Reminder Time",
	"input_respon_time"	=>  "Input Response Time",
	"respon_time"	=>  "Response Time",
	"response"	=>  "Response",
	"customer"	=>  "Customer",
	"officer"	=>  "Officer",
	"website"	=>  "Website",
	"select"	=>  "-- Select --",
	"active"	=>	"Active",
	"inactive"	=>	"Not active",
	"actions"	=>  "Actions",
	"upload"	=>  "Upload",
	"download"	=>  "Download",
	"file_upload"		=>  "Select file upload:",
	"selectfile_import" => "Select File to Import:",
	"success_import" => "Your Data has successfully imported",
	"error_import" => "Error inserting the data.",
	//Category
	"name" 		=>  "Name",
	"parent" 	=>	"Parent",
	//group user
	"category" 		=>  "Category",
	"company" 	=>	"Company",
	"parent_company" 	=>	"Parent company",
	"address" 	=>	"Address",
	"type" 	=>	"Type",
	//user
	"username" 	=>	"Username",
	"phone" 	=>	"Phone",
	"role" 		=>	"Role",
	"send"		=>	"Send email",
	//menu
	"parent_menu"	=>	"Parent Menu",
	"order" 		=>	"Order",
	"level_menu" 	=>	"Level Menu",
	"icon" 		=>	"Icon",
	"is_link" 	=>	"Is Link",
	//role
	"fullname" 	=>	"Name",
	"role_name"	=>	"Role As",

];