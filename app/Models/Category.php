<?php

namespace App\Models;

use Baum\Node;

class Category extends Node
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'categories';

    protected $fillable = ['id', 'parent_id', 'lft', 'rgt', 'depth', 'name', 'slug', 'published'];
}
