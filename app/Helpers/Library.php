<?php
namespace App\Helpers;

class Library {
    
    /**
     * Get active sidebar menu
     */
    function active($path) {
        $routeName = Route::currentRouteName();
        return (substr($routeName, 0, strpos($routeName, ".")) == $path) ? 'active' : '';
        // return (request()->is($path)) ? 'active' : '';        
    }

    /**
     * Set active sidebar menu
     */
    function set_active($path) {
        return request()->is($path);
    }

    /**
     * Generate token from string
     */
    function generate_token($name) {
        //generate custom hash sebagai auth token
        $generated_token = base64_encode(sha1(rand(1, 10000) . uniqid() . $name));
        
        return $generated_token;
    }

    /**
     * Create random code number
     */
    function getcodenumber($prefix = 'GTBASE', $id = '0')
    {
        $numberCode = "{$prefix}/".date("Ymd")."/{$id}";
        
        return $numberCode;
    }

    /**
     * Encrypt & Decrypt string by iwan
     * $string : Iwan Prasetiyo
     * $action : e => encrypt, d => decrypt
     * encrypt = wan_crypt('Hello World', 'e');
     * decrypt = wan_crypt('RTlOMytOZStXdjdHbDZtamNDWFpGdz09', 'd');
     */
    function iwanp_crypt($string, $action = 'e') {
        
        $secret_key = 'iwanp_crypt_1234567890';
        $secret_iv = 'iwanp_crypt_secret_iv';
    
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash('sha256', $secret_key);
        $iv = substr(hash( 'sha256', $secret_iv ), 0, 16);
    
        if( $action == 'e' ) {
            $output = base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
        }
        else if( $action == 'd' ){
            $output = openssl_decrypt(base64_decode($string ), $encrypt_method, $key, 0, $iv);
        }
    
        return $output;
    }

    /**
     * Get action route name
     */
    function action_routeName() {
        $action = Route::currentRouteAction();
        $finalAction = str_replace("@","", substr($action, strpos($action, '@')));
        
        switch($finalAction)
        {
            case 'index':
                return 'Action INDEX';
                break;
            case 'create':
                return 'Action CREATE';
                break;
            case 'store':
                return 'Action STORE';
                break;
            case 'show':
                return 'Action SHOW';
                break;
            case 'edit':
                return 'Action EDIT';
                break;
            case 'update':
                return 'Action UPDATE';
                break;
            default:
                return false;
        }
    }

    /**
     * Get action routename full
     */
    function action_routeNameFull() {
        $action = Route::currentRouteAction();
        $finalAction = str_replace("@","", substr($action, strpos($action, '@')));

        $routeName = Route::currentRouteName();
        $finalName = ucwords(str_replace("-"," ",str_replace("."," ",$routeName)));
        
        switch($finalAction)
        {
            case 'index':
                return $finalName;
                break;
            case 'create':
                return $finalName;
                break;
            case 'store':
                return $finalName;
                break;
            case 'show':
                return $finalName;
                break;
            case 'edit':
                return $finalName;
                break;
            case 'update':
                return $finalName;
                break;
            default:
                return false;
        }
    }

}