<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $root = Category::all();
        // $root = new Category();
        $cek = [];
        foreach ($root as $key => $value) {
            $cek[$key] = $value->name;
        }
        dd($cek);
        // $root = Category::create(['name' => 'Root category']);
        // $root = Category::find(1);

        // $child1 = $root->children()->create(['name' => 'Child 3']);
        // dd($child1);

        // return view('home');

    }
}
