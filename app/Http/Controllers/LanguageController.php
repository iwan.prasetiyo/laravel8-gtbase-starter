<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LanguageController extends Controller
{
    public function index($locale = 'id')
    {
        if (!in_array($locale, ['en', 'id'])){
            $locale = 'id';
        }

        // if (! in_array($locale, ['en', 'id'])) {
        //     abort(400);
        // }

        // App::setLocale($locale);
        session()->put('locale', $locale);
        
        return redirect()->back();
    }
}
