<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// localization switch language
Route::get('lang/{locale}', 'LanguageController@index');

Route::get('/', function () {
    return view('welcome');
});

Route::get('category', 'Admin\CategoryController@index');

/* Test Helper */
Route::get('/helper-user/{id}', function ($id) {
    return (UserHelp::get_name($id)) ? UserHelp::get_name($id) : 'User not found!!';
});

Route::get('/helper-function', function () {
    $globalfunc = new FuncHelp();
    $token = $globalfunc->generate_token('Iwan Prasetiyo');
    $code = $globalfunc->getcodenumber('IRCTIRE', '12345');
    dd($globalfunc, $token, $code);
});

/* Sample Page */
Route::get('/animations', function () {
    return view('admin.samplepage.animation');
})->name('animation.sample');
Route::get('/blank', function () {
    return view('admin.samplepage.blank');
})->name('blank.sample');
Route::get('/borders', function () {
    return view('admin.samplepage.border');
})->name('border.sample');
Route::get('/buttons', function () {
    return view('admin.samplepage.button');
})->name('button.sample');
Route::get('/cards', function () {
    return view('admin.samplepage.card');
})->name('card.sample');
Route::get('/charts', function () {
    return view('admin.samplepage.chart');
})->name('chart.sample');
Route::get('/colors', function () {
    return view('admin.samplepage.color');
})->name('color.sample');
Route::get('/others', function () {
    return view('admin.samplepage.other');
})->name('other.sample');
Route::get('/tables', function () {
    return view('admin.samplepage.table');
})->name('table.sample');
Route::get('/404', function () {
    return view('admin.samplepage.errors404');
})->name('404.sample');

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home', 'Admin\DashboardController@index')->name('home');
// Route::get('/profile', 'ProfileController@index')->name('profile');
// Route::put('/profile', 'ProfileController@update')->name('profile.update');
